package response_old

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"

	"gitlab.com/teterski-softworks/gopnik/v2/elements/video/youtube/embeddedvideo"
	"gitlab.com/teterski-softworks/logging/v2/errortrace"
)

//Response is a YouTube API response to any given request.
type Response struct {
	Kind  string `json:"kind"`
	Items []item `json:"items"`
}

type item struct {
	Kind    string  `json:"kind"`
	ID      string  `json:"id"`
	Snippet snippet `json:"snippet"`
}

type snippet struct {
	Thumbnails  thumbnails `json:"thumbnails"`
	Title       string     `json:"Title"`
	Description string     `json:"description"`
	ResourceID  resourceID `json:"resourceId"`
}

type resourceID struct {
	Kind    string `json:"kind"`
	VideoID string `json:"videoId"`
}

type thumbnails struct {
	Medium medium `json:"medium"`
	Maxres maxres `json:"maxres"`
}

type medium struct {
	URL string `json:"url"`
}
type maxres struct {
	URL string `json:"url"`
}

func getAPIKey() (string, errortrace.ErrorTracer) {
	variableTitle := "YOUTUBEAPI_KEY"
	key := os.Getenv(variableTitle)
	if key == "" {
		return "", errortrace.MissingAPIKey(variableTitle)
	}
	return key, nil
}

//TODO!: Fix this to the latest Google API
//GetVideo returns an embedded YouTube video based on the video ID provided.
func GetVideo(id string) (embeddedvideo.EmbeddedVideo, errortrace.ErrorTracer) {

	req, err1 := http.NewRequest("GET", "https://www.googleapis.com/youtube/v3/videos", nil)
	if err1 != nil {
		return embeddedvideo.EmbeddedVideo{}, errortrace.New(err1)
	}

	query := req.URL.Query()

	var key string
	key, err2 := getAPIKey()
	if err2 != nil {
		return embeddedvideo.EmbeddedVideo{}, err2
	}

	query.Add("key", key)
	query.Add("id", id)
	query.Add("part", "snippet")
	req.URL.RawQuery = query.Encode()

	resp, err3 := getResponse(req)
	if err3 != nil {
		return embeddedvideo.EmbeddedVideo{}, errortrace.New(err3)
	}

	items := resp.Items

	return items[0].embed()
}

//GetPlaylist returns a list of embedded YouTube videos based on the playlist ID provided.
func GetPlaylist(playlistID string, maxResult int) ([]embeddedvideo.EmbeddedVideo, errortrace.ErrorTracer) {

	req, err1 := http.NewRequest("GET", "https://www.googleapis.com/youtube/v3/playlistItems", nil)
	if err1 != nil {
		return []embeddedvideo.EmbeddedVideo{}, errortrace.New(err1)
	}

	query := req.URL.Query()

	key, err2 := getAPIKey()
	if err2 != nil {
		return nil, err2
	}
	query.Add("key", key)
	query.Add("playlistId", playlistID)
	query.Add("maxResults", strconv.Itoa(maxResult))
	query.Add("part", "snippet")
	req.URL.RawQuery = query.Encode()

	resp, err3 := getResponse(req)
	if err3 != nil {
		return nil, err3
	}
	items := resp.Items
	var videos []embeddedvideo.EmbeddedVideo
	for _, item := range items {
		vid, err := item.embed()
		if err != nil {
			return videos, err
		}
		videos = append(videos, vid)
	}
	return videos, nil

}

//getResponse gets a YouTube API response based on the HTTP request being send.
func getResponse(req *http.Request) (Response, errortrace.ErrorTracer) {

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return Response{}, errortrace.New(err)
	}
	if resp == nil {
		return Response{}, errortrace.New(err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return Response{}, errortrace.New(err)
	}
	fmt.Printf("TEST: %v\nERROR: %v\n", body, err)
	var response Response
	err = json.Unmarshal(body, &response)
	if err != nil {
		return Response{}, errortrace.New(err)
	}

	defer resp.Body.Close()

	return response, nil
}

//embed - Converts an item struct received via the YouTube API, to an EmbeddedVideo
func (item *item) embed() (embeddedvideo.EmbeddedVideo, errortrace.ErrorTracer) {
	video, err := embeddedvideo.New()
	if err != nil {
		return video, err
	}

	video.Title = item.Snippet.Title

	if item.Kind == "youtube#video" {
		video.URL = item.ID
	} else if item.Kind == "youtube#playlistItem" {
		video.URL = item.Snippet.ResourceID.VideoID
	} else {
		err = errortrace.New(fmt.Errorf("no match found for Item.Kind"))
		video.URL = ""
	}

	video.Thumbnail = item.Snippet.Thumbnails.Maxres.URL

	return video, err
}
