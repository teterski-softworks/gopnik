package footer

import (
	"net/http"
	"time"

	"gitlab.com/teterski-softworks/gopnik/v2/attribution"
	"gitlab.com/teterski-softworks/gopnik/v2/htmltemplate"
	"gitlab.com/teterski-softworks/logging/v2/errortrace"
)

//Footer is the footer of a webpage.
type Footer struct {
	Year int

	htmltemplate.HTMLTemplate
	attribution.Attribution
}

//TODO
func New() (Footer, errortrace.ErrorTracer) {
	f := Footer{}
	if err := htmltemplate.UseDefault(&f); err != nil {
		return f, err
	}
	f.Attribution.SetClass("footer")
	return f, nil
}

//Execute prints the Footer to a web browser.
func (f Footer) Execute(w *http.ResponseWriter) errortrace.ErrorTracer {

	f.Year = time.Now().Year()

	if err := htmltemplate.ApplyTemplate(w, &f); err != nil {
		return err
	}

	return nil
}
