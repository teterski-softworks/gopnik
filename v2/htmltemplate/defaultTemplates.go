package htmltemplate

import (
	"reflect"
	"strings"
	"text/template"

	"gitlab.com/teterski-softworks/gopnik/v2/errors"
	"gitlab.com/teterski-softworks/logging/v2/errortrace"
)

var defaultTemplates = make(map[string]*template.Template)

//SetDefault sets the default template for the type of "i"
func SetDefault(i interface{}, path string) errortrace.ErrorTracer {
	var err error
	var t *template.Template
	if t, err = template.ParseFiles(path); err != nil {
		return errortrace.New(err)
	}
	key := formatKey(i)
	defaultTemplates[key] = t

	return nil
}

//Default gets the default template for the type of "i". If no template is set, it returns nil.
func Default(i interface{}) *template.Template {
	key := formatKey(i)
	return defaultTemplates[key]
}

//RemoveDefault removes the default template set for "i"
func RemoveDefault(i interface{}) {
	key := formatKey(i)
	delete(defaultTemplates, key)
}

//formatKey returns the Title of "i" as a string. It removes any "*" if i is a pointer.
func formatKey(i interface{}) string {
	return strings.Replace(reflect.TypeOf(i).String(), "*", "", -1)
}

//UseDefault sets "i" to use the default HTML template previously set for "i".
func UseDefault(i HTMLTemplater) errortrace.ErrorTracer {
	defaultTemplate := Default(i)

	if defaultTemplate == nil {
		return errors.NoHTMLTemplate()
	}

	return i.SetTemplate(defaultTemplate)
}

//ClearAllDefaults removes all the default templates previously set. Use cautiously.
func ClearAllDefaults() {
	defaultTemplates = make(map[string]*template.Template)
}
