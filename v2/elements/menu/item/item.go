package item

import (
	"net/http"

	"gitlab.com/teterski-softworks/gopnik/v2/attribution"
	"gitlab.com/teterski-softworks/gopnik/v2/htmltemplate"
	"gitlab.com/teterski-softworks/logging/v2/errortrace"
)

//Item is a link that is displayed inside a menu.
type Item struct {
	Text string
	Link string
	Icon string

	htmltemplate.HTMLTemplate
	attribution.Attribution
}

//TODO
func New() (Item, errortrace.ErrorTracer) {
	i := Item{}
	if err := htmltemplate.UseDefault(&i); err != nil {
		return i, err
	}
	i.Attribution.SetClass("menu item")
	return i, nil
}

//Execute prints the Item into a web browser.
func (i Item) Execute(w *http.ResponseWriter) errortrace.ErrorTracer {

	if err := htmltemplate.ApplyTemplate(w, &i); err != nil {
		return err
	}

	return nil
}
