package page

import (
	"fmt"
	"net/http"

	"gitlab.com/teterski-softworks/gopnik/v2/elements/layout/flexbox"
	"gitlab.com/teterski-softworks/gopnik/v2/elements/layout/footer"
	"gitlab.com/teterski-softworks/gopnik/v2/elements/layout/header"
	"gitlab.com/teterski-softworks/gopnik/v2/elements/layout/orientation"
	"gitlab.com/teterski-softworks/gopnik/v2/execution"
	"gitlab.com/teterski-softworks/logging/v2/errortrace"
)

//Page is a webpage.
type Page struct {
	header.Header
	Body flexbox.Flexbox
	footer.Footer

	execution.Execution
}

//TODO
func New() (Page, errortrace.ErrorTracer) {

	p := Page{}
	var err errortrace.ErrorTracer

	if p.Header, err = header.New(); err != nil {
		return p, err
	}
	p.Body = flexbox.New(orientation.Horizontal)
	p.Body.SetClass("body")
	if p.Footer, err = footer.New(); err != nil {
		return p, err
	}

	return p, nil

}

//Execute prints the Page to a web browser.
func (page Page) Execute(w *http.ResponseWriter) errortrace.ErrorTracer {

	fmt.Fprintf(*w, "<html>")

	if err := page.Header.Execute(w); err != nil {
		return err
	}
	if err := page.Body.Execute(w); err != nil {
		return err
	}
	if err := page.Footer.Execute(w); err != nil {
		return err
	}

	fmt.Fprintf(*w, "</html>")

	return nil
}

//Append adds Executers to the Page's body.
func (page *Page) Append(e execution.Executer) {
	page.Body.Append(e)
}
