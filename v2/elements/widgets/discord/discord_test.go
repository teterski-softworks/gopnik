package discord_test

import (
	"net/http"
	"reflect"
	"testing"

	"gitlab.com/teterski-softworks/gopnik/v2/attribution"
	"gitlab.com/teterski-softworks/gopnik/v2/elements/widgets/discord"
	"gitlab.com/teterski-softworks/gopnik/v2/execution"
	"gitlab.com/teterski-softworks/gopnik/v2/htmltemplate"
	"gitlab.com/teterski-softworks/gopnik/v2/testinghelpers"
	extraHelpers "gitlab.com/teterski-softworks/testinghelpers"
)

func Test_Discord(t *testing.T) {
	extraHelpers.Implements(&discord.Discord{}, (*htmltemplate.HTMLTemplater)(nil), t)
}

func Test_Discord_New(t *testing.T) {
	htmltemplate.ClearAllDefaults()

	d, err := discord.New("", discord.Light)
	if err == nil {
		t.Fatal(err)
	}
	if err := htmltemplate.SetDefault(d, "./testdata/templates/discord.goxml"); err != nil {
		t.Fatal(err)
	}
	d, err = discord.New("", discord.Light)
	if err != nil {
		t.Fatal(err)
	}

	template, err := htmltemplate.NewFromTemplate(htmltemplate.Default(d))
	if err != nil {
		t.Fatal(err)
	}

	var attribution attribution.Attribution
	attribution.AddClass("widgets discord")

	test := discord.Discord{
		Theme:        discord.Light,
		HTMLTemplate: template,
		Attribution:  attribution,
	}

	if d != test {
		t.Fatalf("\n%s.Construct()\nExpected:\n%v\nActual:\n%v\n\n", reflect.TypeOf(d), test, d)
	}
}
func Test_Discord_Execute(t *testing.T) {

	if err := htmltemplate.SetDefault(discord.Discord{}, "./testdata/templates/discord.goxml"); err != nil {
		t.Fatal(err)
	}

	handler := func(w http.ResponseWriter, str string) execution.Executer {
		d, err := discord.New("", discord.Light)
		if err != nil {
			t.Fatal(err)
		}
		d.ServerID = str
		d.Classes = str
		d.ID = str
		d.Theme = true
		if err := d.Execute(&w); err != nil {
			t.Fatal(err)
		}
		return &d
	}

	expectedResult :=
		"<Discord><ServerID>{{}}</ServerID><Theme>true</Theme><Classes>{{}}</Classes><ID>{{}}</ID></Discord>"

	testinghelpers.TestExecuteHelper(t, handler, expectedResult)
	testinghelpers.TestExecuteFailureHelper(t, &discord.Discord{})
}
