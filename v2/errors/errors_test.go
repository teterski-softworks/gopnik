package errors_test

import (
	"testing"

	"gitlab.com/teterski-softworks/gopnik/v2/errors"
)

func Test_Errors_MissingAPIKey(t *testing.T) {

	text := "could not retrieve API key from environment variable TEST"
	err := errors.MissingAPIKey("TEST")

	if !err.Fatal() {
		t.Errorf("\nMissingAPIKey() \nExpected: %v Actual: %v\n\n", false, err.Fatal())
	}
	if err.Error() != text {
		t.Errorf("\nMissingAPIKey() Text does not match.\nExpected: %v\nActual: %v\n\n", text, err.Error())
	}

}

func Test_Errors_NoHTMLTemplate(t *testing.T) {

	text := "no HTML template set, cannot execute"
	err := errors.NoHTMLTemplate()

	if err.Fatal() {
		t.Errorf("\nNoHTMLTemplate() Fatal is incorrect.\nExpected: %v Actual: %v\n\n", false, err.Fatal())
	}
	if err.Error() != text {
		t.Errorf("\nNoHTMLTemplate() Text does not match.\nExpected: %v\nActual: %v\n\n", text, err.Error())
	}

}
