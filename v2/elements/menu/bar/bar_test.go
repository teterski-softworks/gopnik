package bar_test

import (
	"net/http"
	"reflect"
	"testing"

	"gitlab.com/teterski-softworks/gopnik/v2/attribution"
	"gitlab.com/teterski-softworks/gopnik/v2/elements/menu/bar"
	"gitlab.com/teterski-softworks/gopnik/v2/elements/menu/item"
	"gitlab.com/teterski-softworks/gopnik/v2/execution"
	"gitlab.com/teterski-softworks/gopnik/v2/htmltemplate"
	"gitlab.com/teterski-softworks/gopnik/v2/testinghelpers"
	extraHelpers "gitlab.com/teterski-softworks/testinghelpers"
)

func Test_Bar(t *testing.T) {
	extraHelpers.Implements(&bar.Bar{}, (*htmltemplate.HTMLTemplater)(nil), t)
}

func Test_Bar_New(t *testing.T) {
	htmltemplate.ClearAllDefaults()

	b, err := bar.New()
	if err == nil {
		t.Fatal(err)
	}
	if err := htmltemplate.SetDefault(b, "./testdata/templates/bar.goxml"); err != nil {
		t.Fatal(err)
	}
	b, err = bar.New()
	if err != nil {
		t.Fatal(err)
	}

	template, err := htmltemplate.NewFromTemplate(htmltemplate.Default(b))
	var attribution attribution.Attribution
	attribution.AddClass("menu bar")

	test := bar.Bar{
		HTMLTemplate: template,
		Attribution:  attribution,
	}

	if !reflect.DeepEqual(b, test) {
		t.Fatalf("\n%s.Construct()\nExpected:\n%v\nActual:\n%v\n\n", reflect.TypeOf(b), test, b)
	}

}

func Test_Bar_Execute(t *testing.T) {

	if err := htmltemplate.SetDefault(bar.Bar{}, "./testdata/templates/bar.goxml"); err != nil {
		t.Fatal(err)
	}

	if err := htmltemplate.SetDefault(item.Item{}, "../item/testdata/templates/item.goxml"); err != nil {
		t.Fatal(err)
	}

	handler := func(w http.ResponseWriter, str string) execution.Executer {

		b, err := bar.New()
		if err != nil {
			t.Fatal(err)
		}

		b.Classes = str
		b.ID = str

		nested, err := item.New()
		if err != nil {
			t.Fatal(err)
		}

		nested.Text = str
		nested.Link = str
		nested.Icon = str
		nested.ID = str
		b.Append(&nested)
		if err := b.Execute(&w); err != nil {
			t.Fatal(err)
		}

		return &b
	}

	expectedResult :=
		"<Bar><Classes>{{}}</Classes><ID>{{}}</ID></Bar><Item><Icon>{{}}</Icon><Classes>menu item</Classes><ID>{{}}</ID><Text>{{}}</Text><Link>{{}}</Link></Item></div>\n"

	testinghelpers.TestExecuteHelper(t, handler, expectedResult)
	testinghelpers.TestExecuteFailureHelper(t, bar.Bar{})
	b, err := bar.New()
	if err != nil {
		t.Fatal(err)
	}
	b.Append(&item.Item{})
	testinghelpers.TestExecuteFailureHelper(t, &b)
}
