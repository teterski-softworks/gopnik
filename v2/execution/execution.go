package execution

import (
	"net/http"

	"gitlab.com/teterski-softworks/logging/v2/errortrace"
)

//Executer allows printing into an http.ResponseWriter.
type Executer interface {
	Execute(*http.ResponseWriter) errortrace.ErrorTracer
}

//Execution allows printing into an http.ResponseWriter.
type Execution struct {
}

//TODO
func New() Execution {
	return Execution{}
}

//Execute prints the Execution's content into an http.ResponseWriter.
func (e Execution) Execute(w *http.ResponseWriter) errortrace.ErrorTracer {

	/*
		This method is blank.
		Its simply here to make sure Execution (and any of
		its parents) implement the Executer interface.

	*/
	return nil
}

//Array allows to add Executers to an element.
type Array struct {
	Items []Executer
}

//Append adds an Executer to the ExecuterArray.
func (s *Array) Append(e Executer) {
	s.Items = append(s.Items, e)
}
