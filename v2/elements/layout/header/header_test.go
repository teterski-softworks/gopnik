package header_test

import (
	"net/http"
	"reflect"
	"testing"

	"gitlab.com/teterski-softworks/gopnik/v2/elements/layout/header"
	"gitlab.com/teterski-softworks/gopnik/v2/execution"
	"gitlab.com/teterski-softworks/gopnik/v2/htmltemplate"
	"gitlab.com/teterski-softworks/gopnik/v2/testinghelpers"
	extraHelpers "gitlab.com/teterski-softworks/testinghelpers"
)

func Test_Header(t *testing.T) {
	extraHelpers.Implements(&header.Header{}, (*htmltemplate.HTMLTemplater)(nil), t)
}

func Test_Header_New(t *testing.T) {
	htmltemplate.ClearAllDefaults()
	h, err := header.New()
	if err == nil {
		t.Fatal(err)
	}
	if err := htmltemplate.SetDefault(h, "./testdata/templates/header.goxml"); err != nil {
		t.Fatal(err)
	}
	h, err = header.New()
	if err != nil {
		t.Fatal(err)
	}

	template, err := htmltemplate.NewFromTemplate(htmltemplate.Default(h))

	test := header.Header{
		HTMLTemplate: template,
	}
	if h != test {
		t.Fatalf("\n%s.Construct()\nExpected:\n%v\nActual:\n%v\n\n", reflect.TypeOf(h), test, h)
	}
}

func Test_Header_Execute(t *testing.T) {

	if err := htmltemplate.SetDefault(header.Header{}, "./testdata/templates/header.goxml"); err != nil {
		t.Fatal(err)
	}

	handler := func(w http.ResponseWriter, str string) execution.Executer {

		h, err := header.New()
		if err != nil {
			t.Fatal(err)
		}

		h.Title = str
		h.Classes = str
		h.ID = str
		h.CSS = str
		h.Favicon = str
		h.Execute(&w)
		return &h
	}

	expectedResult :=
		"<Head><Favicon>{{}}</Favicon><CSS>{{}}</CSS><Title>{{}}</Title></Head>"

	testinghelpers.TestExecuteHelper(t, handler, expectedResult)
	testinghelpers.TestExecuteFailureHelper(t, &header.Header{})
}
