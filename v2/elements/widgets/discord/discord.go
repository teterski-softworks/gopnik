package discord

import (
	"net/http"

	"gitlab.com/teterski-softworks/gopnik/v2/attribution"
	"gitlab.com/teterski-softworks/gopnik/v2/htmltemplate"
	"gitlab.com/teterski-softworks/logging/v2/errortrace"
)

//Discord widget displays a Discord server.
type Discord struct {
	ServerID string

	Theme Theme //light - true , dark - false

	htmltemplate.HTMLTemplate
	attribution.Attribution
}

//TODO
type Theme bool

const (
	//TODO
	Light Theme = true
	//TODO
	Dark Theme = !Light
)

//TODO
func New(serverID string, theme Theme) (Discord, errortrace.ErrorTracer) {
	d := Discord{
		ServerID: serverID,
		Theme:    theme,
	}
	if err := htmltemplate.UseDefault(&d); err != nil {
		return d, err
	}
	d.Attribution.SetClass("widgets discord")
	return d, nil
}

//Execute prints the Discord widget into a web browser.
func (d Discord) Execute(w *http.ResponseWriter) errortrace.ErrorTracer {

	if err := htmltemplate.ApplyTemplate(w, &d); err != nil {
		return err
	}

	return nil
}
