package errors

import (
	"fmt"

	"gitlab.com/teterski-softworks/logging/v2/errortrace"
)

//NoHTMLTemplate is an error thrown if an object has no HTML template set to it.
func NoHTMLTemplate() errortrace.ErrorTracer {
	return errortrace.New(fmt.Errorf("no HTML template set, cannot execute"))
}

//MissingAPIKey is an error thrown if an API key is missing.
func MissingAPIKey(variableTitle string) errortrace.ErrorTracer {
	err := errortrace.New(fmt.Errorf("could not retrieve API key from environment variable %s", variableTitle))
	err.SetFatal(true)
	return err
}
