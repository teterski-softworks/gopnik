package header

import (
	"net/http"

	"gitlab.com/teterski-softworks/gopnik/v2/attribution"
	"gitlab.com/teterski-softworks/gopnik/v2/htmltemplate"
	"gitlab.com/teterski-softworks/logging/v2/errortrace"
)

//Header is the head of a webpage.
type Header struct {
	Title   string
	Favicon string
	CSS     string

	htmltemplate.HTMLTemplate
	attribution.Attribution
}

//TODO
func New() (Header, errortrace.ErrorTracer) {
	h := Header{}
	if err := htmltemplate.UseDefault(&h); err != nil {
		return h, err
	}
	return h, nil

}

//Execute prints the Head to a web browser.
func (h Header) Execute(w *http.ResponseWriter) errortrace.ErrorTracer {

	if err := htmltemplate.ApplyTemplate(w, &h); err != nil {
		return err
	}

	return nil
}
