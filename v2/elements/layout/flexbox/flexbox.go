package flexbox

import (
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/teterski-softworks/gopnik/v2/attribution"
	"gitlab.com/teterski-softworks/gopnik/v2/elements/layout/orientation"
	"gitlab.com/teterski-softworks/gopnik/v2/execution"
	"gitlab.com/teterski-softworks/logging/v2/errortrace"
)

//Flexbox groups together Executions in either a horizontal or vertical fashion.
type Flexbox struct {
	Orientation orientation.Orientation

	attribution.Attribution
	execution.Execution
	execution.Array
}

//TODO
func New(direction orientation.Orientation) Flexbox {
	f := Flexbox{}
	f.setOrientation(direction)
	return f
}

//TODO: Redo, as we no longer need to worry about f.Classes having any values.
//setOrientation sets the Flexbox's orientation.
func (f *Flexbox) setOrientation(direction orientation.Orientation) {
	f.Orientation = direction

	if f.Classes == "" {
		//if no classes are yet set, we set it arbitrarily to one of the flexbox classes
		//this class will then be replaced if need be in the later lines of code.
		f.SetClass("flexbox-col")
	}
	if f.Orientation == orientation.Horizontal {
		f.SetClass(strings.Replace(f.Classes, "flexbox-col", "flexbox-row", -1))
	} else {
		f.SetClass(strings.Replace(f.Classes, "flexbox-row", "flexbox-col", -1))
	}
}

//Execute prints the Flexbox to a web browser.
func (f Flexbox) Execute(w *http.ResponseWriter) errortrace.ErrorTracer {

	fmt.Fprintf(*w, "<div id=\"%s\" class=\"%s\">", f.ID, f.Classes)

	for i := range f.Items {
		if err := f.Items[i].Execute(w); err != nil {
			return err
		}
	}

	fmt.Fprint(*w, "</div>")

	return nil

}
