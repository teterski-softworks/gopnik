package twitchstream_test

import (
	"net/http"
	"reflect"
	"testing"

	"gitlab.com/teterski-softworks/gopnik/v2/attribution"
	"gitlab.com/teterski-softworks/gopnik/v2/elements/video/twitchstream"
	"gitlab.com/teterski-softworks/gopnik/v2/execution"
	"gitlab.com/teterski-softworks/gopnik/v2/htmltemplate"
	"gitlab.com/teterski-softworks/gopnik/v2/testinghelpers"
	extraHelpers "gitlab.com/teterski-softworks/testinghelpers"
)

func Test_Twitchstream(t *testing.T) {
	extraHelpers.Implements(&twitchstream.TwitchStream{}, (*htmltemplate.HTMLTemplater)(nil), t)
}

func Test_Twitchstream_New(t *testing.T) {
	htmltemplate.ClearAllDefaults()

	l, err := twitchstream.New("")
	if err == nil {
		t.Fatal(err)
	}
	if err := htmltemplate.SetDefault(l, "./testdata/templates/twitchstream.goxml"); err != nil {
		t.Fatal(err)
	}
	l, err = twitchstream.New("")
	if err != nil {
		t.Fatal(err)
	}

	template, err := htmltemplate.NewFromTemplate(htmltemplate.Default(l))
	if err != nil {
		t.Fatal(err)
	}

	var attribution attribution.Attribution
	attribution.AddClass("twitchstream")

	test := twitchstream.TwitchStream{
		HTMLTemplate: template,
		Attribution:  attribution,
	}

	if l != test {
		t.Fatalf("\n%s.Construct()\nExpected:\n%v\nActual:\n%v\n\n", reflect.TypeOf(l), test, l)
	}
}

func Test_Twitchstream_Execute(t *testing.T) {

	if err := htmltemplate.SetDefault(twitchstream.TwitchStream{}, "./testdata/templates/twitchstream.goxml"); err != nil {
		t.Fatal(err)
	}

	handler := func(w http.ResponseWriter, str string) execution.Executer {
		l, err := twitchstream.New("")
		if err != nil {
			t.Fatal(err)
		}
		l.Username = str
		l.Classes = str
		l.ID = str
		if err := l.Execute(&w); err != nil {
			t.Fatal(err)
		}
		return &l
	}

	expectedResult :=
		"<LiveStream><Username>{{}}</Username><Classes>{{}}</Classes><ID>{{}}</ID></LiveStream>"

	testinghelpers.TestExecuteHelper(t, handler, expectedResult)
	testinghelpers.TestExecuteFailureHelper(t, &twitchstream.TwitchStream{})
}
