package embeddedvideo

import (
	"net/http"

	"gitlab.com/teterski-softworks/gopnik/v2/attribution"
	"gitlab.com/teterski-softworks/gopnik/v2/htmltemplate"
	"gitlab.com/teterski-softworks/logging/v2/errortrace"
)

//EmbeddedVideo displays an embedded YouTube video.
type EmbeddedVideo struct {
	Title     string
	URL       string
	Thumbnail string

	htmltemplate.HTMLTemplate
	attribution.Attribution
}

//TODO
func New() (EmbeddedVideo, errortrace.ErrorTracer) {
	v := EmbeddedVideo{}
	if err := htmltemplate.UseDefault(&v); err != nil {
		return v, err
	}
	v.Attribution.SetClass("youtube video")
	return v, nil
}

//Execute prints the EmbeddedVideo into a web browser.
func (v EmbeddedVideo) Execute(w *http.ResponseWriter) errortrace.ErrorTracer {

	if err := htmltemplate.ApplyTemplate(w, &v); err != nil {
		return err
	}

	return nil
}
