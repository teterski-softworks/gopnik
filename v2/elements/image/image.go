package image

import (
	"net/http"

	"gitlab.com/teterski-softworks/gopnik/v2/attribution"
	"gitlab.com/teterski-softworks/gopnik/v2/htmltemplate"
	"gitlab.com/teterski-softworks/logging/v2/errortrace"
)

//Image displays an image file.
type Image struct {
	Text      string
	Link      string
	ImagePath string

	htmltemplate.HTMLTemplate
	attribution.Attribution
}

//TODO
func New() (Image, errortrace.ErrorTracer) {
	i := Image{}
	if err := htmltemplate.UseDefault(&i); err != nil {
		return i, err
	}
	i.Attribution.SetClass("image")
	return i, nil
}

//Execute prints the Image into a web browser.
func (i Image) Execute(w *http.ResponseWriter) errortrace.ErrorTracer {

	if err := htmltemplate.ApplyTemplate(w, &i); err != nil {
		return err
	}

	return nil
}
