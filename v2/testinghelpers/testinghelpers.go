package testinghelpers

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"reflect"
	"strings"
	"testing"

	"gitlab.com/teterski-softworks/gopnik/v2/execution"
	"gitlab.com/teterski-softworks/testinghelpers"
)

//TestExecuteHelper is used in testing the Execute method.
func TestExecuteHelper(t *testing.T, handler func(http.ResponseWriter, string) execution.Executer, expectedResult string) {

	for _, test := range testinghelpers.TestingTableStrings {

		expectedResult := strings.ReplaceAll(expectedResult, "{{}}", test.Expected)

		writer := httptest.NewRecorder()
		executer := handler(writer, test.Input)
		response := writer.Result()

		if response.StatusCode != 200 {
			t.Fatal("\n%s.Execute() Status code expected 200, actual %d", reflect.TypeOf(executer), response.StatusCode)
		}

		result, err := ioutil.ReadAll(response.Body)
		if err != nil {
			t.Fatal("\n%s.Execute() Unable to read response body. Error: %s", reflect.TypeOf(executer), err)
		}
		//This is the data the Executer wrote during execution. It has been converted to a string of XML using the template defined in the handler.
		actualResult := string(result)

		//We want to make sure that the XML strings of both the data sent and the data received are equal.
		if expectedResult != actualResult {
			t.Fatal("\n%s.Execute()\nExpected:\n%s\nActual:\n%s\n\n", reflect.TypeOf(executer), expectedResult, actualResult)
		}
	}
}

//TestExecuteFailureHelper is used in testing error recovery of the Execute method
func TestExecuteFailureHelper(t *testing.T, e execution.Executer) {
	w := (http.ResponseWriter)(httptest.NewRecorder())

	if err := e.Execute(&w); err == nil {
		t.Fatal("\n%s.Execute() did not properly throw error.", reflect.TypeOf(e))
	}
}
