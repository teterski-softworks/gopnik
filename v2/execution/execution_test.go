package execution_test

import (
	"net/http"
	"testing"

	"gitlab.com/teterski-softworks/gopnik/v2/execution"
	"gitlab.com/teterski-softworks/testinghelpers"
)

func Test_Array_Append(t *testing.T) {
	var a execution.Array

	var tests = []execution.Execution{
		execution.New(),
		execution.New(),
		execution.New(),
	}
	for i, e := range tests {

		a.Append(&e)
		if a.Items == nil {
			t.Error("Executer did not get appended.")
		}
		if len(a.Items) != i+1 {
			t.Fatalf("Mismatching array length. Expected %d, Actual: %d", i+1, len(a.Items))
		}
	}

}

func Test_Executer(t *testing.T) {
	testinghelpers.Implements(&execution.Execution{}, (*execution.Executer)(nil), t)
}

func Test_Execution_Execute(t *testing.T) {
	w := (*http.ResponseWriter)(nil)
	e := execution.New()
	if err := e.Execute(w); err != nil {
		t.Fatalf("Execution did not run, even though it was ready. Error: %s", err)
	}
}
