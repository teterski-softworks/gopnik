package twitchstream

import (
	"net/http"

	"gitlab.com/teterski-softworks/gopnik/v2/attribution"
	"gitlab.com/teterski-softworks/gopnik/v2/htmltemplate"
	"gitlab.com/teterski-softworks/logging/v2/errortrace"
)

//TwitchStream displays an embedded Twitch live stream.
type TwitchStream struct {
	Username string

	htmltemplate.HTMLTemplate
	attribution.Attribution
}

//TODO
func New(username string) (TwitchStream, errortrace.ErrorTracer) {

	t := TwitchStream{Username: username}

	if err := htmltemplate.UseDefault(&t); err != nil {
		return t, err
	}
	t.SetClass("twitchstream")
	return t, nil
}

//Execute prints the LiveStream into a web browser.
func (t TwitchStream) Execute(w *http.ResponseWriter) errortrace.ErrorTracer {

	if err := htmltemplate.ApplyTemplate(w, &t); err != nil {
		return err
	}

	return nil
}
