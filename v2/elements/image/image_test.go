package image_test

import (
	"net/http"
	"reflect"
	"testing"

	"gitlab.com/teterski-softworks/gopnik/v2/attribution"
	"gitlab.com/teterski-softworks/gopnik/v2/elements/image"
	"gitlab.com/teterski-softworks/gopnik/v2/execution"
	"gitlab.com/teterski-softworks/gopnik/v2/htmltemplate"
	"gitlab.com/teterski-softworks/gopnik/v2/testinghelpers"
	extraHelpers "gitlab.com/teterski-softworks/testinghelpers"
)

func Test_Image(t *testing.T) {
	extraHelpers.Implements(&image.Image{}, (*htmltemplate.HTMLTemplater)(nil), t)
}

func Test_Image_New(t *testing.T) {
	htmltemplate.ClearAllDefaults()

	i, err := image.New()
	if err == nil {
		t.Fatal(err)
	}
	if err := htmltemplate.SetDefault(i, "./testdata/templates/image.goxml"); err != nil {
		t.Fatal(err)
	}
	i, err = image.New()
	if err != nil {
		t.Fatal(err)
	}

	template, err := htmltemplate.NewFromTemplate(htmltemplate.Default(i))
	if err != nil {
		t.Fatal(err)
	}
	var attribution attribution.Attribution
	attribution.AddClass("image")

	test := image.Image{
		HTMLTemplate: template,
		Attribution:  attribution,
	}

	if i != test {
		t.Fatalf("\n%s.Construct()\nExpected:\n%v\nActual:\n%v\n\n", reflect.TypeOf(i), test, i)
	}
}

func Test_Image_Execute(t *testing.T) {

	if err := htmltemplate.SetDefault(image.Image{}, "./testdata/templates/image.goxml"); err != nil {
		t.Fatal(err)
	}

	handler := func(w http.ResponseWriter, str string) execution.Executer {

		i, err := image.New()
		if err != nil {
			t.Fatal(err)
		}

		i.Classes = str
		i.ID = str
		i.Text = str
		i.Link = str
		i.ImagePath = str
		if err := i.Execute(&w); err != nil {
			t.Fatal(err)
		}
		return &i
	}

	expectedResult :=
		"<Image><Classes>{{}}</Classes><ID>{{}}</ID><Text>{{}}</Text><Link>{{}}</Link><ImagePath>{{}}</ImagePath></Image>"

	testinghelpers.TestExecuteHelper(t, handler, expectedResult)
	testinghelpers.TestExecuteFailureHelper(t, image.Image{})
}
