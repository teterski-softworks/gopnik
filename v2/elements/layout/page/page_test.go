package page_test

import (
	"net/http"
	"reflect"
	"testing"

	"gitlab.com/teterski-softworks/gopnik/v2/elements/layout/flexbox"
	"gitlab.com/teterski-softworks/gopnik/v2/elements/layout/footer"
	"gitlab.com/teterski-softworks/gopnik/v2/elements/layout/header"
	"gitlab.com/teterski-softworks/gopnik/v2/elements/layout/orientation"
	"gitlab.com/teterski-softworks/gopnik/v2/elements/layout/page"
	"gitlab.com/teterski-softworks/gopnik/v2/elements/paragraph"
	"gitlab.com/teterski-softworks/gopnik/v2/execution"
	"gitlab.com/teterski-softworks/gopnik/v2/htmltemplate"
	"gitlab.com/teterski-softworks/gopnik/v2/testinghelpers"
	extraHelpers "gitlab.com/teterski-softworks/testinghelpers"
)

func Test_Page(t *testing.T) {
	extraHelpers.Implements(&page.Page{}, (*execution.Executer)(nil), t)
}

func Test_Page_New(t *testing.T) {
	htmltemplate.ClearAllDefaults()

	p, err := page.New()
	if err == nil {
		t.Fatal(err)
	}

	if err := htmltemplate.SetDefault(header.Header{}, "../header/testdata/templates/header.goxml"); err != nil {
		t.Fatal(err)
	}
	if err := htmltemplate.SetDefault(footer.Footer{}, "../footer/testdata/templates/footer.goxml"); err != nil {
		t.Fatal(err)
	}

	p, err = page.New()
	if err != nil {
		t.Fatal(err)
	}

	header, err := header.New()
	if err != nil {
		t.Fatal(err)
	}
	body := flexbox.New(orientation.Horizontal)
	body.SetClass("body")
	footer, err := footer.New()
	if err != nil {
		t.Fatal(err)
	}
	exe := execution.New()

	var test = page.Page{
		Header:    header,
		Body:      body,
		Footer:    footer,
		Execution: exe,
	}

	if !reflect.DeepEqual(p, test) {
		t.Fatalf("\n%s.New()\nExpected:\n%v\nActual:\n%v\n\n", reflect.TypeOf(p), test, p)
	}

}

func Test_Page_New_Errors(t *testing.T) {
	htmltemplate.ClearAllDefaults()

	_, err := page.New()
	if err == nil {
		t.Fatalf("\npage.New() Did not error out properly.")
	}
	if err = htmltemplate.SetDefault(header.Header{}, "../header/testdata/templates/header.goxml"); err != nil {
		t.Fatal(err)
	}

	_, err = page.New()
	if err == nil {
		t.Fatalf("\npage.New() Did not error out properly.")
	}

	if err = htmltemplate.SetDefault(footer.Footer{}, "../footer/testdata/templates/footer.goxml"); err != nil {
		t.Fatal(err)
	}

	_, err = page.New()
	if err != nil {
		t.Fatal(err)
	}

}

func Test_Page_Execute(t *testing.T) {

	if err := htmltemplate.SetDefault(header.Header{}, "../header/testdata/templates/header.goxml"); err != nil {
		t.Fatal(err)
	}
	if err := htmltemplate.SetDefault(footer.Footer{}, "../footer/testdata/templates/footer.goxml"); err != nil {
		t.Fatal(err)
	}
	if err := htmltemplate.SetDefault(paragraph.Paragraph{}, "../../paragraph/testdata/templates/paragraph.goxml"); err != nil {
		t.Fatal(err)
	}

	handler := func(w http.ResponseWriter, str string) execution.Executer {
		p, err := page.New()
		if err != nil {
			t.Fatal(err)
		}
		p.CSS = str
		p.Favicon = str
		p.Header.Title = str
		p.Body.Classes = str
		p.Body.ID = str
		p.Footer.Classes = str
		p.Footer.ID = str

		nested, err := paragraph.New()
		if err != nil {
			t.Fatal(err)
		}
		nested.Title = str
		nested.Text = str
		nested.Note = str
		nested.Link = str
		nested.ID = str
		nested.Classes = str
		p.Append(&nested)
		if err := p.Execute(&w); err != nil {
			t.Fatalf("%v", err.Stack())
		}

		return &p
	}

	expectedResult :=
		"<html><Head><Favicon>{{}}</Favicon><CSS>{{}}</CSS><Title>{{}}</Title></Head><div id=\"{{}}\" class=\"{{}}\"><Paragraph><Note>{{}}</Note><Classes>{{}}</Classes><ID>{{}}</ID><Title>{{}}</Title><Text>{{}}</Text><Link>{{}}</Link></Paragraph></div><Footer><Year>0</Year><Classes>{{}}</Classes><ID>{{}}</ID></Footer></html>"

	testinghelpers.TestExecuteHelper(t, handler, expectedResult)

	var p page.Page
	testinghelpers.TestExecuteFailureHelper(t, &p)
	p, err := page.New()
	if err != nil {
		t.Fatal(err)
	}

	p.Footer = footer.Footer{}
	testinghelpers.TestExecuteFailureHelper(t, &p)

	p.Body = flexbox.Flexbox{}
	testinghelpers.TestExecuteFailureHelper(t, &p)

	p.Header = header.Header{}
	testinghelpers.TestExecuteFailureHelper(t, &p)

}
