package twitter_test

import (
	"net/http"
	"reflect"
	"testing"

	"gitlab.com/teterski-softworks/gopnik/v2/attribution"
	"gitlab.com/teterski-softworks/gopnik/v2/elements/widgets/twitter"
	"gitlab.com/teterski-softworks/gopnik/v2/execution"
	"gitlab.com/teterski-softworks/gopnik/v2/htmltemplate"
	"gitlab.com/teterski-softworks/gopnik/v2/testinghelpers"
	extraHelpers "gitlab.com/teterski-softworks/testinghelpers"
)

func Test_Twitter(t *testing.T) {
	extraHelpers.Implements(&twitter.Twitter{}, (*htmltemplate.HTMLTemplater)(nil), t)
}

func Test_Twitter_New(t *testing.T) {
	htmltemplate.ClearAllDefaults()

	tw, err := twitter.New("")
	if err == nil {
		t.Fatal(err)
	}
	if err := htmltemplate.SetDefault(tw, "./testdata/templates/twitter.goxml"); err != nil {
		t.Fatal(err)
	}
	tw, err = twitter.New("")
	if err != nil {
		t.Fatal(err)
	}

	template, err := htmltemplate.NewFromTemplate(htmltemplate.Default(tw))
	if err != nil {
		t.Fatal(err)
	}

	var attribution attribution.Attribution
	attribution.AddClass("widgets twitter")

	test := twitter.Twitter{
		HTMLTemplate: template,
		Attribution:  attribution,
	}

	if tw != test {
		t.Fatalf("\n%s.Construct()\nExpected:\n%v\nActual:\n%v\n\n", reflect.TypeOf(tw), test, tw)
	}
}

func Test_Twitter_Execute(t *testing.T) {

	if err := htmltemplate.SetDefault(twitter.Twitter{}, "./testdata/templates/twitter.goxml"); err != nil {
		t.Fatal(err)
	}

	handler := func(w http.ResponseWriter, str string) execution.Executer {
		tw, err := twitter.New("")
		if err != nil {
			t.Fatal(err)
		}
		tw.Classes = str
		tw.ID = str
		tw.Username = str
		tw.Execute(&w)
		return &tw
	}

	expectedResult :=
		"<Twitter><Username>{{}}</UserTitle><Classes>{{}}</Classes><ID>{{}}</ID></Twitter>"

	testinghelpers.TestExecuteHelper(t, handler, expectedResult)
	testinghelpers.TestExecuteFailureHelper(t, &twitter.Twitter{})

}
