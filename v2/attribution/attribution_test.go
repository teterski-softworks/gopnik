package attribution_test

import (
	"testing"

	"gitlab.com/teterski-softworks/gopnik/v2/attribution"
	"gitlab.com/teterski-softworks/testinghelpers"
)

func TestAddClass(t *testing.T) {
	var a attribution.Attribution

	var tests = []struct {
		Input    string
		Expected string
	}{
		{"", ""},
		{"a", "a"},
		{"1", "a 1"},
		{"Test", "a 1 Test"},
		{"", "a 1 Test"},
		{"2Test", "a 1 Test 2Test"},
		{"Test with spaces", "a 1 Test 2Test Test with spaces"},
		{"ячмтьбю.", "a 1 Test 2Test Test with spaces ячмтьбю."},
	}

	for _, test := range tests {
		a.AddClass(test.Input)
		if a.Classes != test.Expected {
			t.Fatalf("AddClass(%s): expected %s, actual %s", test.Input, test.Expected, a.Classes)
		}
	}

}

func TestSetClass(t *testing.T) {

	var a attribution.Attribution

	var tests = testinghelpers.TestingTableStrings

	for _, test := range tests {
		a.SetClass(test.Input)
		if a.Classes != test.Expected {
			t.Fatalf("SetClass(%s): expected %s, actual %s", test.Input, test.Expected, a.Classes)
		}
	}
}

func TestSetID(t *testing.T) {
	var a attribution.Attribution

	var tests = testinghelpers.TestingTableStrings

	for _, test := range tests {
		a.SetID(test.Input)
		if a.ID != test.Input {
			t.Fatalf("SetID(%s): expected %s, actual %s", test.Input, test.Expected, a.ID)
		}
	}

}
