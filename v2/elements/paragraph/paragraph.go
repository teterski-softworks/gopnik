package paragraph

import (
	"net/http"

	"gitlab.com/teterski-softworks/gopnik/v2/attribution"
	"gitlab.com/teterski-softworks/gopnik/v2/htmltemplate"
	"gitlab.com/teterski-softworks/logging/v2/errortrace"
)

//Paragraph displays a block of text.
type Paragraph struct {
	Title string
	Text  string
	Link  string
	Note  string

	htmltemplate.HTMLTemplate
	attribution.Attribution
}

//TODO
func New() (Paragraph, errortrace.ErrorTracer) {
	p := Paragraph{}
	if err := htmltemplate.UseDefault(&p); err != nil {
		return p, err
	}
	p.SetClass("paragraph")
	return p, nil
}

//Execute prints the Paragraph into a web browser.
func (p Paragraph) Execute(w *http.ResponseWriter) errortrace.ErrorTracer {

	if err := htmltemplate.ApplyTemplate(w, &p); err != nil {
		return err
	}

	return nil
}
