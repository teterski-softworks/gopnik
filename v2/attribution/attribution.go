package attribution

//Attribution allows an element to be given CSS IDs and classes.
type Attribution struct {
	Classes string
	ID      string
}

//SetClass sets the class of the Selector. This overrides any existing classes.
func (a *Attribution) SetClass(class string) {
	a.Classes = class
}

//AddClass adds a CSS class to the Selector.
func (a *Attribution) AddClass(class string) {
	if class == "" {
		return
	}
	if a.Classes == "" {
		a.Classes = class
		return
	}
	a.Classes = a.Classes + " " + class
}

//SetID sets the HTML/CSS ID of the Selector.
func (a *Attribution) SetID(ID string) {
	a.ID = ID
}
