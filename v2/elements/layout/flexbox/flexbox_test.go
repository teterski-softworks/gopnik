package flexbox_test

import (
	"net/http"
	"reflect"
	"testing"

	"gitlab.com/teterski-softworks/gopnik/v2/attribution"
	"gitlab.com/teterski-softworks/gopnik/v2/elements/layout/flexbox"
	"gitlab.com/teterski-softworks/gopnik/v2/elements/layout/orientation"
	"gitlab.com/teterski-softworks/gopnik/v2/elements/paragraph"
	"gitlab.com/teterski-softworks/gopnik/v2/execution"
	"gitlab.com/teterski-softworks/gopnik/v2/htmltemplate"
	"gitlab.com/teterski-softworks/gopnik/v2/testinghelpers"
	extraHelpers "gitlab.com/teterski-softworks/testinghelpers"
)

func Test_Flexbox(t *testing.T) {
	extraHelpers.Implements(&flexbox.Flexbox{}, (*execution.Executer)(nil), t)
}

func Test_Flexbox_New(t *testing.T) {
	s := flexbox.New(orientation.Vertical)

	exe := execution.New()

	var attribution attribution.Attribution
	attribution.SetClass("flexbox-col")

	test := flexbox.Flexbox{
		Orientation: orientation.Vertical,
		Execution:   exe,
		Attribution: attribution,
	}
	if !reflect.DeepEqual(s, test) {
		t.Fatalf("\n%s.Construct()\nExpected:\n%v\nActual:\n%v\n\n", reflect.TypeOf(s), test, s)
	}

}

func Test_Flexbox_Execute(t *testing.T) {

	if err := htmltemplate.SetDefault(paragraph.Paragraph{}, "../../paragraph/testdata/templates/paragraph.goxml"); err != nil {
		t.Fatal(err)
	}

	handler := func(w http.ResponseWriter, str string) execution.Executer {

		f := flexbox.New(orientation.Vertical)
		f.Classes = str
		f.ID = str

		nested, err := paragraph.New()
		if err != nil {
			t.Fatal(err)
		}
		nested.Title = str
		nested.Classes = str
		nested.ID = str
		nested.Note = str
		nested.Text = str
		nested.Link = str
		f.Append(&nested)
		if err := f.Execute(&w); err != nil {
			t.Fatal(err)
		}

		return &f
	}

	expectedResult :=
		"<div id=\"{{}}\" class=\"{{}}\"><Paragraph><Note>{{}}</Note><Classes>{{}}</Classes><ID>{{}}</ID><Title>{{}}</Title><Text>{{}}</Text><Link>{{}}</Link></Paragraph></div>"

	testinghelpers.TestExecuteHelper(t, handler, expectedResult)

	f := flexbox.New(orientation.Vertical)
	f.Append(&paragraph.Paragraph{})
	testinghelpers.TestExecuteFailureHelper(t, &f)
}
