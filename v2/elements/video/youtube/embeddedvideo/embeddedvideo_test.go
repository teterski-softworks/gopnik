package embeddedvideo_test

import (
	"net/http"
	"reflect"
	"testing"

	"gitlab.com/teterski-softworks/gopnik/v2/attribution"
	"gitlab.com/teterski-softworks/gopnik/v2/elements/video/youtube/embeddedvideo"
	"gitlab.com/teterski-softworks/gopnik/v2/execution"
	"gitlab.com/teterski-softworks/gopnik/v2/htmltemplate"
	"gitlab.com/teterski-softworks/gopnik/v2/testinghelpers"
	extraHelpers "gitlab.com/teterski-softworks/testinghelpers"
)

func Test_EmbeddedVideo(t *testing.T) {
	extraHelpers.Implements(&embeddedvideo.EmbeddedVideo{}, (*htmltemplate.HTMLTemplater)(nil), t)
}

func Test_EmbeddedVideo_New(t *testing.T) {
	htmltemplate.ClearAllDefaults()

	ev, err := embeddedvideo.New()
	if err == nil {
		t.Fatal(err)
	}
	if err := htmltemplate.SetDefault(ev, "./testdata/templates/embeddedVideo.goxml"); err != nil {
		t.Fatal(err)
	}
	ev, err = embeddedvideo.New()
	if err != nil {
		t.Fatal(err)
	}

	template, err := htmltemplate.NewFromTemplate(htmltemplate.Default(ev))
	if err != nil {
		t.Fatal(err)
	}

	var attribution attribution.Attribution
	attribution.AddClass("youtube video")

	test := embeddedvideo.EmbeddedVideo{
		HTMLTemplate: template,
		Attribution:  attribution,
	}

	if ev != test {
		t.Fatalf("\n%s.Construct()\nExpected:\n%v\nActual:\n%v\n\n", reflect.TypeOf(ev), test, ev)
	}
}

func Test_EmbeddedVideo_Execute(t *testing.T) {

	if err := htmltemplate.SetDefault(embeddedvideo.EmbeddedVideo{}, "./testdata/templates/embeddedVideo.goxml"); err != nil {
		t.Fatal(err)
	}

	handler := func(w http.ResponseWriter, str string) execution.Executer {
		ev, err := embeddedvideo.New()
		if err != nil {
			t.Fatal(err)
		}
		ev.Classes = str
		ev.ID = str
		ev.Thumbnail = str
		ev.URL = str
		ev.Title = str
		if err := ev.Execute(&w); err != nil {
			t.Fatal(err)
		}
		return &ev
	}

	expectedResult :=
		"<EmbeddedVideo><URL>{{}}</URL><Thumbnail>{{}}</Thumbnail><Classes>{{}}</Classes><ID>{{}}</ID><Title>{{}}</Title></EmbeddedVideo>"

	testinghelpers.TestExecuteHelper(t, handler, expectedResult)
	testinghelpers.TestExecuteFailureHelper(t, &embeddedvideo.EmbeddedVideo{})
}
