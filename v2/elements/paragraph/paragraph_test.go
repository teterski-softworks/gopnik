package paragraph_test

import (
	"net/http"
	"reflect"
	"testing"

	"gitlab.com/teterski-softworks/gopnik/v2/attribution"
	"gitlab.com/teterski-softworks/gopnik/v2/elements/paragraph"
	"gitlab.com/teterski-softworks/gopnik/v2/execution"
	"gitlab.com/teterski-softworks/gopnik/v2/htmltemplate"
	"gitlab.com/teterski-softworks/gopnik/v2/testinghelpers"
	extraHelpers "gitlab.com/teterski-softworks/testinghelpers"
)

func Test_Paragraph(t *testing.T) {
	extraHelpers.Implements(&paragraph.Paragraph{}, (*htmltemplate.HTMLTemplater)(nil), t)
}

func Test_Paragraph_New(t *testing.T) {
	htmltemplate.ClearAllDefaults()

	p, err := paragraph.New()
	if err == nil {
		t.Fatal(err)
	}
	if err = htmltemplate.SetDefault(p, "./testdata/templates/paragraph.goxml"); err != nil {
		t.Fatal(err)
	}
	p, err = paragraph.New()
	if err != nil {
		t.Fatal(err)
	}

	template, err := htmltemplate.NewFromTemplate(htmltemplate.Default(p))
	if err != nil {
		t.Fatal(err)
	}
	var attribution attribution.Attribution
	attribution.AddClass("paragraph")

	test := paragraph.Paragraph{
		HTMLTemplate: template,
		Attribution:  attribution,
	}

	if p != test {
		t.Fatalf("\n%s.Construct()\nExpected:\n%v\nActual:\n%v\n\n", reflect.TypeOf(p), test, p)
	}
}

func Test_Paragraph_Execute(t *testing.T) {

	if err := htmltemplate.SetDefault(paragraph.Paragraph{}, "./testdata/templates/paragraph.goxml"); err != nil {
		t.Fatal(err)
	}

	handler := func(w http.ResponseWriter, str string) execution.Executer {
		p, err := paragraph.New()
		if err != nil {
			t.Fatal(err)
		}
		p.Classes = str
		p.ID = str
		p.Title = str
		p.Text = str
		p.Link = str
		p.Note = str
		if err := p.Execute(&w); err != nil {
			t.Fatal(err)
		}
		return &p
	}

	expectedResult :=
		"<Paragraph><Note>{{}}</Note><Classes>{{}}</Classes><ID>{{}}</ID><Title>{{}}</Title><Text>{{}}</Text><Link>{{}}</Link></Paragraph>"

	testinghelpers.TestExecuteHelper(t, handler, expectedResult)
	testinghelpers.TestExecuteFailureHelper(t, paragraph.Paragraph{})
}
