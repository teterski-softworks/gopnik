package embeddedplaylist

import (
	"net/http"

	"gitlab.com/teterski-softworks/gopnik/v2/attribution"
	"gitlab.com/teterski-softworks/logging/v2/errortrace"

	"gitlab.com/teterski-softworks/gopnik/v2/htmltemplate"
)

//EmbeddedPlaylist displays an embedded Twitch live stream.
type EmbeddedPlaylist struct {
	PlaylistID string
	Thumbnail  string

	htmltemplate.HTMLTemplate
	attribution.Attribution
}

//TODO
func New() (EmbeddedPlaylist, errortrace.ErrorTracer) {
	p := EmbeddedPlaylist{}
	if err := htmltemplate.UseDefault(&p); err != nil {
		return p, err
	}
	p.SetClass("youtube playlist")
	return p, nil
}

//Execute prints the LiveStream into a web browser.
func (p EmbeddedPlaylist) Execute(w *http.ResponseWriter) errortrace.ErrorTracer {

	if err := htmltemplate.ApplyTemplate(w, &p); err != nil {
		return err
	}

	return nil
}
