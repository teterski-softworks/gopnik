package messages

import (
	"fmt"
	"gitlab.com/teterski-softworks/logging/v2/message"
)

//ServerStart is a message that states the server has started up.
func ServerStart(port string) messager.Messager {
	return New(fmt.Sprintf("Server started. Listening and serving on port %s", port))
}

//ServerStop is a message that states the server has stopped running.
func ServerStop() messager.Messager {
	m := New("Server stopped.")
	m.SetFatal(true)
	m.SetExitCode(0)
	return m
}
