package dropdown_test

import (
	"net/http"
	"reflect"
	"testing"

	"gitlab.com/teterski-softworks/gopnik/v2/elements/menu/dropdown"
	"gitlab.com/teterski-softworks/gopnik/v2/elements/menu/item"
	"gitlab.com/teterski-softworks/gopnik/v2/execution"
	"gitlab.com/teterski-softworks/gopnik/v2/htmltemplate"
	"gitlab.com/teterski-softworks/gopnik/v2/testinghelpers"
	extraHelpers "gitlab.com/teterski-softworks/testinghelpers"
)

func Test_DropDown(t *testing.T) {
	extraHelpers.Implements(&dropdown.Dropdown{}, (*htmltemplate.HTMLTemplater)(nil), t)
}

func Test_DropDown_New(t *testing.T) {
	htmltemplate.ClearAllDefaults()

	s, err := dropdown.New()
	if err == nil {
		t.Fatal(err)
	}
	if err := htmltemplate.SetDefault(s, "./testdata/templates/dropDown.goxml"); err != nil {
		t.Fatal(err)
	}
	s, err = dropdown.New()
	if err != nil {
		t.Fatal(err)
	}

	if err := htmltemplate.SetDefault(item.Item{}, "./testdata/templates/dropDown.goxml"); err != nil {
		t.Fatal(err)
	}
	i, err := item.New()
	if err != nil {
		t.Fatal(err)
	}
	i.SetClass("menu dropDown")

	test := dropdown.Dropdown{
		Item: i,
	}

	if !reflect.DeepEqual(s, test) {
		t.Fatalf("\n%s.Construct()\nExpected:\n%v\nActual:\n%v\n\n", reflect.TypeOf(s), test, s)
	}
}

func Test_DropDown_Execute(t *testing.T) {

	if err := htmltemplate.SetDefault(dropdown.Dropdown{}, "./testdata/templates/dropDown.goxml"); err != nil {
		t.Fatal(err)
	}

	if err := htmltemplate.SetDefault(item.Item{}, "../item/testdata/templates/item.goxml"); err != nil {
		t.Fatal(err)
	}

	handler := func(w http.ResponseWriter, str string) execution.Executer {

		d, err := dropdown.New()
		if err != nil {
			t.Fatal(err)
		}

		d.Classes = str
		d.ID = str
		d.Link = str
		d.Text = str
		d.Icon = str

		nested, err := item.New()
		if err != nil {
			t.Fatal(err)
		}
		nested.Text = str
		nested.Link = str
		nested.Icon = str
		nested.ID = str
		d.Append(&nested)
		if err := d.Execute(&w); err != nil {
			t.Fatal(err)
		}

		return &d
	}

	expectedResult :=
		"<DropDown><Classes>{{}}</Classes><ID>{{}}</ID><Link>{{}}</Link><Text>{{}}</Text><Icon>{{}}</Icon></DropDown><Item><Icon>{{}}</Icon><Classes>menu item</Classes><ID>{{}}</ID><Text>{{}}</Text><Link>{{}}</Link></Item></a>\n"

	testinghelpers.TestExecuteHelper(t, handler, expectedResult)
	testinghelpers.TestExecuteFailureHelper(t, &dropdown.Dropdown{})
	d, err := dropdown.New()
	if err != nil {
		t.Fatal(err)
	}
	d.Append(&item.Item{})
	testinghelpers.TestExecuteFailureHelper(t, &d)

}
