module gitlab.com/teterski-softworks/gopnik/v2

go 1.14

require (
	gitlab.com/teterski-softworks/logging/v2 v2.0.1
	gitlab.com/teterski-softworks/testinghelpers v1.4.0
)
