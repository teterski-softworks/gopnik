package response_test

import (
	"fmt"
	"testing"

	"gitlab.com/teterski-softworks/gopnik/v2/elements/video/youtube/embeddedvideo"
	"gitlab.com/teterski-softworks/gopnik/v2/elements/video/youtube/response"
)

//TODO!: Code coverage is only 83.7%

func Test_GetVideo(t *testing.T) {
	id := "dQw4w9WgXcQ"

	s, err := response.GetVideo(id)
	if err != nil {
		t.Fatal(err)
	}
	test, err := embeddedvideo.New()
	if err != nil {
		t.Fatal(err)
	}
	test.URL = id
	test.Thumbnail = fmt.Sprintf("https://i.ytimg.com/vi/%s/maxresdefault.jpg", id)
	test.Title = "Rick Astley - Never Gonna Give You Up (Video)"

	if s != test {
		t.Fatalf("\nGetVideo(%s)\nExpected:\n%v\nActual:\n%v\n\n", id, test, s)
	}
}

func Test_GetPlaylist(t *testing.T) {
	id := "OLAK5uy_kCPBrqbyz3Dn_g1aLMs3lYvt09XLWKpgk"

	s, err := response.GetPlaylist(id, 4)
	if err != nil {
		t.Fatal(err)
	}
	var tests [4]embeddedvideo.EmbeddedVideo

	if tests[0], err = response.GetVideo("dQw4w9WgXcQ"); err != nil {
		t.Fatal(err)
	}
	if tests[1], err = response.GetVideo("BeyEGebJ1l4"); err != nil {
		t.Fatal(err)
	}
	if tests[2], err = response.GetVideo("yPYZpwSpKmA"); err != nil {
		t.Fatal(err)
	}
	if tests[3], err = response.GetVideo("VBssFzSx6sI"); err != nil {
		t.Fatal(err)
	}

	for i := range tests {

		if s[i] != tests[i] {
			t.Fatalf("GetPlaylist(%s): expected %v, actual %v", id, tests[i], s[i])
		}
	}
}
