package orientation

//Orientation consists of two values, horizontal and vertical
type Orientation bool

const (
	//Horizontal orientation
	Horizontal Orientation = false
	//Vertical orientation
	Vertical Orientation = true
)
