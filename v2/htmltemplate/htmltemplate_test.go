package htmltemplate_test

import (
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
	"text/template"

	"gitlab.com/teterski-softworks/gopnik/v2/execution"
	"gitlab.com/teterski-softworks/gopnik/v2/htmltemplate"
	"gitlab.com/teterski-softworks/gopnik/v2/testinghelpers"
	extraHelpers "gitlab.com/teterski-softworks/testinghelpers"
)

func Test_HTMLTemplater(t *testing.T) {
	extraHelpers.Implements(&htmltemplate.HTMLTemplate{}, (*htmltemplate.HTMLTemplater)(nil), t)
}

//TODO: MAKE A New() test. and remove testing for error when using New in other methods.

func Test_HTMLTemplater_ApplyTemplate(t *testing.T) {

	//For this test we simply want to make sure that the HTTP mocking is working.
	//We want to make sure we get a status code 200

	handler := func(w http.ResponseWriter, str string) execution.Executer {

		h, err := htmltemplate.New("./testdata/templates/testtemplate.goxml")
		if err != nil {
			t.Fatal(err)
		}
		if err := htmltemplate.ApplyTemplate(&w, &h); err != nil {
			t.Fatal(err)
		}
		return &h
	}

	testinghelpers.TestExecuteHelper(t, handler, "<Name>testtemplate.goxml</Name>")

	w := (http.ResponseWriter)(httptest.NewRecorder())
	h, err := htmltemplate.New("./testdata/templates/testtemplate2.goxml")
	if err != nil {
		t.Fatal(err)
	}
	if err := htmltemplate.ApplyTemplate(&w, &h); err == nil {
		t.Fatalf("\nApplyTemplate() did not properly throw error for invalid template structure.")
	}
	if _, err := htmltemplate.New("invalidpath\\/"); err == nil {
		t.Fatalf("\nApplyTemplate() did not properly throw error for invalid file path.")
	}
}

func Test_HTMLTemplater_Execute(t *testing.T) {
	h, err := htmltemplate.New("./testdata/templates/testtemplate.goxml")
	if err != nil {
		t.Fatal(err)
	}
	w := (http.ResponseWriter)(httptest.NewRecorder())
	if err := h.Execute(&w); err != nil {
		t.Fatalf("\n%s.Execute() %s.", reflect.TypeOf(h), err.Error())
	}
}

func Test_HTMLTemplater_SetHTMLTemplatePath(t *testing.T) {

	h, err := htmltemplate.New("./testdata/templates/testtemplate.goxml")
	if err != nil {
		t.Fatal(err)
	}
	if err := h.SetTemplatePath("Invalid path \\/"); err == nil {
		t.Error("SetTemplatePath() did not throw error for invalid path.")
	}
}

func Test_HTMLTemplater_SetHTMLTemplate(t *testing.T) {

	var h htmltemplate.HTMLTemplate
	var test *template.Template
	if err := h.SetTemplate(test); err == nil {
		t.Error("SetTemplate() did not throw error for invalid path.")
	}
	if err := h.SetTemplate(template.New("./testdata/templates/testtemplate.goxml")); err != nil {
		t.Fatal(err)
	}
}

func Test_HTMLTemplater_Name(t *testing.T) {

	//We do not care for this error, as we test for in another test.
	s, _ := htmltemplate.New("./testdata/templates/testtemplate.goxml")
	if s.Name() != "testtemplate.goxml" {
		t.Fatalf("GetHTMLTemplate() did not return correct template. Expected: %s. Actual: %s", s.Name(), "testTemplate.goxml")
	}

}

func Test_Default(t *testing.T) {
	h := &htmltemplate.HTMLTemplate{}
	if err := htmltemplate.UseDefault(h); err == nil {
		t.Error("UseDefault() did not throw error.")
	}
	if err := htmltemplate.SetDefault(h, "invalid path \\/"); err == nil {
		t.Error("SetDefault() did not properly throw error.")
	}
	if err := htmltemplate.SetDefault(h, "./testdata/templates/testtemplate.goxml"); err != nil {
		t.Fatal(err)
	}
	if htmltemplate.Default(h).Name() != "testtemplate.goxml" {
		t.Error("SetDeafult() did not properly add default template.")
	}
	if err := htmltemplate.UseDefault(h); err != nil {
		t.Error("UseDeafult() threw an error.")
	}
	if h.Name() != "testtemplate.goxml" {
		t.Error("UseDeafult() did not properly set the default template.")
	}
	htmltemplate.RemoveDefault(h)
	if htmltemplate.Default(h) != nil {
		t.Error("RemoveDefault() did not properly remove the template.")
	}

	if err := htmltemplate.SetDefault(h, "./testdata/templates/testtemplate.goxml"); err != nil {
		t.Fatal(err)
	}
	if err := htmltemplate.SetDefault(execution.Execution{}, "./testdata/templates/testtemplate.goxml"); err != nil {
		t.Fatal(err)
	}
	htmltemplate.ClearAllDefaults()
	if htmltemplate.Default(h) != nil {
		t.Error("ClearAllDefaults() did not properly remove all default templates.")
	}
	if htmltemplate.Default(execution.Execution{}) != nil {
		t.Error("ClearAllDefaults() did not properly remove all default templates.")
	}

}
