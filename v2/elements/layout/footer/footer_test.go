package footer_test

import (
	"net/http"
	"reflect"
	"testing"

	"gitlab.com/teterski-softworks/gopnik/v2/attribution"
	"gitlab.com/teterski-softworks/gopnik/v2/elements/layout/footer"
	"gitlab.com/teterski-softworks/gopnik/v2/execution"
	"gitlab.com/teterski-softworks/gopnik/v2/htmltemplate"
	"gitlab.com/teterski-softworks/gopnik/v2/testinghelpers"
	extraHelpers "gitlab.com/teterski-softworks/testinghelpers"
)

func Test_Footer(t *testing.T) {
	extraHelpers.Implements(&footer.Footer{}, (*htmltemplate.HTMLTemplater)(nil), t)
}

func Test_Footer_New(t *testing.T) {
	htmltemplate.ClearAllDefaults()

	f, err := footer.New()
	if err == nil {
		t.Fatal(err)
	}
	htmltemplate.SetDefault(f, "./testdata/templates/footer.goxml")
	f, err = footer.New()
	if err != nil {
		t.Fatal(err)
	}

	template, err := htmltemplate.NewFromTemplate(htmltemplate.Default(f))
	if err != nil {
		t.Fatal(err)
	}
	var attribution attribution.Attribution
	attribution.AddClass("footer")

	test := footer.Footer{
		HTMLTemplate: template,
		Attribution:  attribution,
	}

	if f != test {
		t.Fatalf("\n%s.Construct()\nExpected:\n%v\nActual:\n%v\n\n", reflect.TypeOf(f), test, f)
	}
}
func Test_Footer_Execute(t *testing.T) {

	if err := htmltemplate.SetDefault(footer.Footer{}, "./testdata/templates/footer.goxml"); err != nil {
		t.Fatal(err)
	}

	handler := func(w http.ResponseWriter, str string) execution.Executer {
		f, err := footer.New()
		if err != nil {
			t.Fatal(err)
		}

		f.Classes = str
		f.ID = str
		f.Execute(&w)
		return &f
	}

	expectedResult :=
		"<Footer><Year>0</Year><Classes>{{}}</Classes><ID>{{}}</ID></Footer>"

	testinghelpers.TestExecuteHelper(t, handler, expectedResult)
	testinghelpers.TestExecuteFailureHelper(t, &footer.Footer{})
}
