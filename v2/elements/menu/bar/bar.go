package bar

import (
	"fmt"
	"net/http"

	"gitlab.com/teterski-softworks/gopnik/v2/attribution"
	"gitlab.com/teterski-softworks/gopnik/v2/execution"
	"gitlab.com/teterski-softworks/gopnik/v2/htmltemplate"
	"gitlab.com/teterski-softworks/logging/v2/errortrace"
)

//Bar contains a series of DropDownMenus
type Bar struct {
	htmltemplate.HTMLTemplate
	attribution.Attribution
	execution.Array
}

//TODO
func New() (Bar, errortrace.ErrorTracer) {
	b := Bar{}
	if err := htmltemplate.UseDefault(&b); err != nil {
		return b, err
	}
	b.Attribution.SetClass("menu bar")
	return b, nil
}

//Execute prints the MenuBar into a web browser.
func (b Bar) Execute(w *http.ResponseWriter) errortrace.ErrorTracer {

	if err := htmltemplate.ApplyTemplate(w, &b); err != nil {
		return err
	}

	for i := range b.Items {
		if err := b.Items[i].Execute(w); err != nil {
			return err
		}
	}
	fmt.Fprintln(*w, "</div>") //TODO Remove template? Or make this a bit better inside the template

	return nil
}
