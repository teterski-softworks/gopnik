package htmltemplate

import (
	"net/http"
	"text/template"

	"gitlab.com/teterski-softworks/gopnik/v2/errors"
	"gitlab.com/teterski-softworks/gopnik/v2/execution"
	"gitlab.com/teterski-softworks/logging/v2/errortrace"
)

//HTMLTemplater allows for na object to be templated using an HTML file.
type HTMLTemplater interface {
	execution.Executer
	SetTemplate(*template.Template) errortrace.ErrorTracer
	SetTemplatePath(string) errortrace.ErrorTracer
	Template() *template.Template
	Name() string
}

//HTMLTemplate templates objects that inherit it.
type HTMLTemplate struct {
	execution.Execution
	template *template.Template
}

//TODO
//SetTemplatePath sets the HTML template to be used by the HTMLTemplate using the path provided.
func New(path string) (HTMLTemplate, errortrace.ErrorTracer) {
	t := HTMLTemplate{}
	if err := t.SetTemplatePath(path); err != nil {
		return t, errortrace.New(err)
	}

	return t, nil
}

//TODO:
func NewFromTemplate(tt *template.Template) (HTMLTemplate, errortrace.ErrorTracer) {
	t := HTMLTemplate{}
	if err := t.SetTemplate(tt); err != nil {
		return t, errortrace.New(err)
	}

	return t, nil
}

//SetTemplatePath sets the HTML template to be used by the HTMLTemplate using the path provided.
func (t *HTMLTemplate) SetTemplatePath(path string) errortrace.ErrorTracer {

	var err error
	t.template, err = template.ParseFiles(path)

	if err != nil {
		return errortrace.New(err)
	}

	return nil
}

//SetTemplate sets the HTML template to be used by the HTMLTemplate using a pointer to a Template.
func (t *HTMLTemplate) SetTemplate(tt *template.Template) errortrace.ErrorTracer {
	if tt == nil {
		return errortrace.ErrorNilPointer()
	}
	t.template = tt

	return nil
}

//Template returns the pointer of the template used by HTMLTemplate
func (t HTMLTemplate) Template() *template.Template {
	return t.template
}

//Execute prints the Execution's content into an http.ResponseWriter.
func (t HTMLTemplate) Execute(w *http.ResponseWriter) errortrace.ErrorTracer {

	/*
		This method is blank.
		Its simply here to make sure Execution (and any of
		its parents) implement the Executer interface.

	*/
	return nil
}

//Name returns the name of the template used by the HTMLTemplate
func (t HTMLTemplate) Name() string {
	return t.template.Name()
}

//ApplyTemplate executes the HTMLTemplater using the Template that was previously set.
func ApplyTemplate(w *http.ResponseWriter, t HTMLTemplater) errortrace.ErrorTracer {

	if t.Template() == nil {
		return errors.NoHTMLTemplate()
	}

	if err := t.Template().Execute(*w, t); err != nil {
		return errortrace.New(err)
	}

	return nil
}
