package embeddedplaylist_test

import (
	"net/http"
	"reflect"
	"testing"

	"gitlab.com/teterski-softworks/gopnik/v2/attribution"
	"gitlab.com/teterski-softworks/gopnik/v2/elements/video/youtube/embeddedplaylist"
	"gitlab.com/teterski-softworks/gopnik/v2/execution"
	"gitlab.com/teterski-softworks/gopnik/v2/htmltemplate"
	"gitlab.com/teterski-softworks/gopnik/v2/testinghelpers"
	extraHelpers "gitlab.com/teterski-softworks/testinghelpers"
)

func Test_EmbeddedPlaylist(t *testing.T) {
	extraHelpers.Implements(&embeddedplaylist.EmbeddedPlaylist{}, (*htmltemplate.HTMLTemplater)(nil), t)
}

func Test_EmbeddedPlaylist_New(t *testing.T) {
	htmltemplate.ClearAllDefaults()

	ep, err := embeddedplaylist.New()
	if err == nil {
		t.Fatal(err)
	}
	if err := htmltemplate.SetDefault(ep, "./testdata/templates/embeddedPlaylist.goxml"); err != nil {
		t.Fatal(err)
	}
	ep, err = embeddedplaylist.New()
	if err != nil {
		t.Fatal(err)
	}

	template, err := htmltemplate.NewFromTemplate(htmltemplate.Default(ep))
	if err != nil {
		t.Fatal(err)
	}

	var attribution attribution.Attribution
	attribution.AddClass("youtube playlist")

	test := embeddedplaylist.EmbeddedPlaylist{
		HTMLTemplate: template,
		Attribution:  attribution,
	}

	if ep != test {
		t.Fatalf("\n%s.Construct()\nExpected:\n%v\nActual:\n%v\n\n", reflect.TypeOf(ep), test, ep)
	}
}

func Test_EmbeddedPlaylist_Execute(t *testing.T) {

	if err := htmltemplate.SetDefault(embeddedplaylist.EmbeddedPlaylist{}, "./testdata/templates/embeddedPlaylist.goxml"); err != nil {
		t.Fatal(err)
	}

	handler := func(w http.ResponseWriter, str string) execution.Executer {
		ep, err := embeddedplaylist.New()
		if err != nil {
			t.Fatal(err)
		}
		ep.Classes = str
		ep.ID = str
		ep.Thumbnail = str
		ep.PlaylistID = str
		if err := ep.Execute(&w); err != nil {
			t.Fatal(err)
		}
		return &ep
	}

	expectedResult :=
		"<EmbeddedPlaylist><PlaylistID>{{}}</PlaylistID><Thumbnail>{{}}</Thumbnail><Classes>{{}}</Classes><ID>{{}}</ID></EmbeddedPlaylist>"

	testinghelpers.TestExecuteHelper(t, handler, expectedResult)
	testinghelpers.TestExecuteFailureHelper(t, &embeddedplaylist.EmbeddedPlaylist{})
}
