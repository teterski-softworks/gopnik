package dropdown

import (
	"fmt"
	"net/http"

	"gitlab.com/teterski-softworks/gopnik/v2/elements/menu/item"
	"gitlab.com/teterski-softworks/gopnik/v2/execution"
	"gitlab.com/teterski-softworks/gopnik/v2/htmltemplate"
	"gitlab.com/teterski-softworks/logging/v2/errortrace"
)

//Dropdown organises a series of Items into a drop down menu.
type Dropdown struct {
	item.Item
	execution.Array
}

//TODO
func New() (Dropdown, errortrace.ErrorTracer) {
	d := Dropdown{}
	if err := htmltemplate.UseDefault(&d); err != nil {
		return d, err
	}
	d.Attribution.SetClass("menu dropDown")
	return d, nil
}

//Execute prints the Dropdown into a web browser.
func (d Dropdown) Execute(w *http.ResponseWriter) errortrace.ErrorTracer {

	if err := htmltemplate.ApplyTemplate(w, &d); err != nil {
		return err
	}

	for i := range d.Items {
		if err := d.Items[i].Execute(w); err != nil {
			return err
		}
	}
	fmt.Fprintln(*w, "</a>")

	return nil
}
