package twitter

import (
	"net/http"

	"gitlab.com/teterski-softworks/gopnik/v2/attribution"
	"gitlab.com/teterski-softworks/gopnik/v2/htmltemplate"
	"gitlab.com/teterski-softworks/logging/v2/errortrace"
)

//Twitter widget displays a Twitter feed.
type Twitter struct {
	Username string

	htmltemplate.HTMLTemplate
	attribution.Attribution
}

//TODO
//TODO: change to inlcude username
func New(username string) (Twitter, errortrace.ErrorTracer) {
	t := Twitter{Username: username}
	if err := htmltemplate.UseDefault(&t); err != nil {
		return t, err
	}
	t.Attribution.SetClass("widgets twitter")
	return t, nil
}

//Execute prints the Twitter widget into a web browser.
func (t Twitter) Execute(w *http.ResponseWriter) errortrace.ErrorTracer {

	if err := htmltemplate.ApplyTemplate(w, &t); err != nil {
		return err
	}

	return nil
}
