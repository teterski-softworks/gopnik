package item_test

import (
	"net/http"
	"reflect"
	"testing"

	"gitlab.com/teterski-softworks/gopnik/v2/attribution"
	"gitlab.com/teterski-softworks/gopnik/v2/elements/menu/item"
	"gitlab.com/teterski-softworks/gopnik/v2/execution"
	"gitlab.com/teterski-softworks/gopnik/v2/htmltemplate"
	"gitlab.com/teterski-softworks/gopnik/v2/testinghelpers"
	extraHelpers "gitlab.com/teterski-softworks/testinghelpers"
)

func Test_Item(t *testing.T) {
	extraHelpers.Implements(&item.Item{}, (*htmltemplate.HTMLTemplater)(nil), t)
}

func Test_Item_New(t *testing.T) {
	htmltemplate.ClearAllDefaults()

	i, err := item.New()
	if err == nil {
		t.Fatal(err)
	}
	if err := htmltemplate.SetDefault(i, "./testdata/templates/item.goxml"); err != nil {
		t.Fatal(err)
	}
	i, err = item.New()
	if err != nil {
		t.Fatal(err)
	}

	template, err := htmltemplate.NewFromTemplate(htmltemplate.Default(i))
	if err != nil {
		t.Fatal(err)
	}
	var attribution attribution.Attribution
	attribution.AddClass("menu item")

	test := item.Item{
		HTMLTemplate: template,
		Attribution:  attribution,
	}

	if i != test {
		t.Fatalf("\n%s.Construct()\nExpected:\n%v\nActual:\n%v\n\n", reflect.TypeOf(i), test, i)
	}
}

func Test_Item_Execute(t *testing.T) {

	if err := htmltemplate.SetDefault(item.Item{}, "./testdata/templates/item.goxml"); err != nil {
		t.Fatal(err)
	}

	handler := func(w http.ResponseWriter, str string) execution.Executer {

		i, err := item.New()
		if err != nil {
			t.Fatal(err)
		}

		i.Icon = str
		i.Link = str
		i.Classes = str
		i.ID = str
		i.Text = str
		if err := i.Execute(&w); err != nil {
			t.Fatal(err)
		}
		return &i
	}

	expectedResult :=
		"<Item><Icon>{{}}</Icon><Classes>{{}}</Classes><ID>{{}}</ID><Text>{{}}</Text><Link>{{}}</Link></Item>"

	testinghelpers.TestExecuteHelper(t, handler, expectedResult)
	testinghelpers.TestExecuteFailureHelper(t, &item.Item{})
}
